package com.fur3ll.friendlist;

public class Friend {

    public String name;
    public String phoneNumber;

    public Friend (String name, String phoneNumber){
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
