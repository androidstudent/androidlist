package com.fur3ll.friendlist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView friendsListView = (ListView) findViewById(R.id.friends_lv);

        ArrayUtil arrayUtil = new ArrayUtil(getResources().
                getStringArray(R.array.friends_names));

        FriendsListAdapter adapter = new FriendsListAdapter(this,
                arrayUtil.getList());

        friendsListView.setAdapter(adapter);
    }
}
