package com.fur3ll.friendlist;

import java.util.ArrayList;

public class ArrayUtil {

    private final Integer randomMax = 999999999;
    private final Integer randomMin = 100000000;
    private String array[];
    private ArrayList<Friend> list;

    public ArrayUtil(String array[]) {
        this.array = array;
        generateList();
    }

    public ArrayList<Friend> getList(){
        return list;
    }

    private ArrayList<Friend> generateList() {

        list = new ArrayList<>();

        for (int i = 1; i < array.length; i++) {
            list.add(new Friend(array[i], Integer.toString(randomNumber())));
        }
        return list;
    }
    
    private int randomNumber() {
        return (int) (Math.random() * randomMax) + randomMin;
    }

}
