package com.fur3ll.friendlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class FriendsListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Friend> friends;

    public FriendsListAdapter(Context context, ArrayList<Friend> friends) {
        this.context = context;
        this.friends = friends;
    }

    @Override
    public int getCount() {
        return friends.size();
    }

    @Override
    public Object getItem(int position) {
        return friends.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_friend,
                    parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Friend currentFriend = (Friend) getItem(position);
        viewHolder.itemName.setText(currentFriend.getName());
        viewHolder.itemPhoneNumber.setText(currentFriend.getPhoneNumber());

        return convertView;
    }

    private class ViewHolder {

        TextView itemName;
        TextView itemPhoneNumber;

        public ViewHolder(View view) {
            itemName = (TextView) view.findViewById(R.id.name_tv);
            itemPhoneNumber = (TextView) view.findViewById(R.id.phone_tv);
        }
    }
}
